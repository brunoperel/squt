<?php 
$sample_directory="./querysamples";
$samples=array();
if (is_dir($sample_directory)) {
    if ($dh = opendir($sample_directory)) {
        while (($file = readdir($dh)) !== false) {
        	$isSql = substr($file, -strlen(".sql")) === ".sql";
        	$isAllTests = $_GET["test"]!=="false";
        	$isSqutTest = strpos($file,"DBI") !== 0;
            if ($isSql && ($isAllTests || $isSqutTest)) {
            	$samples[]=$file;
            }
        }
        closedir($dh);
    }
	sort($samples);
    echo implode(",",$samples);
}
else {
	echo "Error : ".$sample_directory." is not a directory";
}
?>